package com.example.homework4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }
    private fun init(){
        val firstName =intent.extras?.getString("first_name", "")
        val lastName=intent.extras?.getString("last_name","")
        val age =intent.extras?.getInt("age",0)

        firstNameTextView.text=firstName
        lastNameTextView.text=lastName
        ageTextView.text=age.toString()

        returnAddressButton.setOnClickListener(){
            returnAddress()

        }

    }
    private fun returnAddress(){
        val address=addressEditText.text.toString()
        val intent=intent
        intent.putExtra("address", address)
        setResult(RESULT_OK, intent)
        finish()
    }
}