package com.example.homework4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ConditionVariable
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {
    private val addressRequestCode=11


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
    }
    private fun init(){
        openSecondActivityButton.setOnClickListener(){
            openSecondActivity()

        }

    }
    private fun openSecondActivity(){
        val firstName=firstNameEditText.text.toString()
        val lastName=lastNameEditText.text.toString()
        val age=ageEditText.text.toString().toInt()

        val intent=Intent(this, SecondActivity::class.java)
        intent.putExtra("first_name", firstName)
        intent.putExtra("last_name", lastName)
        intent.putExtra("age", age)

        startActivityForResult(intent, addressRequestCode)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode==addressRequestCode && resultCode== RESULT_OK){
            val address=data?.extras?.getString("address", "")
            addressTextView.text=address
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}